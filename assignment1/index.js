/*
Input: "name,age\nRohit,34\nVirat,33"

Output: [
            {name:"Rohit", age:34},
            {name:"Virat", age:33}
        ]

*/

let input = "name,age\nRohit,34\nVirat,33";
let output = [];

input = input.split("\n");

for (let i = 1; i < input.length; i++) {
  let temp = {};
  for (let j = 0; j < input[i].split(",").length; j++) {
    temp[input[0].split(",")[j]] = input[i].split(",")[j];
  }
  output.push(temp);
}

console.log(output);
