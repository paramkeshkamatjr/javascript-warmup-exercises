const obj = {
  key1: {
    key11: {
      key21: "hello",
    },
  },
  key2: {
    key21: "world",
  },
};

function checkForKey(object, key) {
  if (Object.keys(object)[0] === "0") return;
  if (Object.keys(object).includes(key)) {
    return object[key];
  } else {
    const temp = Object.values(object);
    if (temp && temp.length > 0) {
      temp.forEach((obj) => checkForKey(obj, key));
    }
  }
}

console.log(checkForKey(obj, "key21"));
