function calculateVolume() {
  const radius = document.getElementById("radius").value;
  if (!radius) {
    alert("Please enter radius");
    return;
  }
  if (isNaN(radius)) {
    alert("Please enter a number input");
    return;
  }
  const volume = (4 / 3) * Math.PI * Math.pow(radius, 3);
  alert(`Volumne of sphere is ${volume.toFixed(2)} cubic units`);
}
